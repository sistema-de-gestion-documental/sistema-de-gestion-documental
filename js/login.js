/*Roles:
1.-Admin
2.-Secretaria*/
function obtenerListaUsuarios(){
var listaUsuarios=JSON.parse(localStorage.getItem('listaUsuariosLs'));
if (listaUsuarios ==null){
    listaUsuarios = [
        ['U1','Romina','info@romina.com', 'Roma','1234', 'Admin'],
        ['U2', 'Frasco asd', 'frasco@mail.com', 'giraver1','12345', 'Secretaria'],
    ]
}
return listaUsuarios;
}

function validarCredenciales(usuario, contrasena){
    var listaUsuarios =obtenerListaUsuarios();
    var bacceso=false;
    for(var i=0; i< listaUsuarios.length; i++){
        if(usuario== listaUsuarios[i][3] && contrasena == listaUsuarios[i][4]){
            bacceso=true;
            sessionStorage.setItem('usuarioActivo', listaUsuarios[i][1] );
            sessionStorage.setItem('rolUsuarioActivo', listaUsuarios[i][5]);
        }
    }
    return bacceso;
}

/*Ingreso */

/*se crea el evento para el boton de ingreso

*/

document.querySelector('#btningresar').addEventListener('click',iniciarSesion);
function iniciarSesion(){
    var susuario='';
    var scontrasena=''; 
   
    var bacceso=false;

    susuario =document.querySelector('#usuario').value;
    scontrasena =document.querySelector('#contrasena').value;

    bacceso=validarCredenciales(susuario,scontrasena)
    if(bacceso== true){
        /*si la variable bacceso está en true llama a la función ingresar */
        ingresar();
    } else{
        alert("Ingrese bien las credenciales porfavor");
    }
}
function ingresar(){
    var rol= sessionStorage.getItem('rolUsuarioActivo');
    /*evalua el rol del usuario */
    switch (rol){
        case 'Admin':
            window.location.href="index.html";
            break;
        case 'Secretaria':
            window.location.href="secretaria.html";
            break;
            default:
                window.location.href="login.html";
                break;

    }
}

/*validar contraseña de olvidocontraseña */

function validarcontra(){
    var contrasenanue, confirmarcontra;
    contrasenanue = document.querySelector('#contrasenanue').value;
    confirmarcontra = document.querySelector('#confirmarcontra').value;
   
    if(confirmarcontra != contrasenanue ){
        alert("Las contraseñas no coinciden");
        return false;
        
    }else{
        alert("Contraseñas restablecidas");
            
        

    }
     
}
     

